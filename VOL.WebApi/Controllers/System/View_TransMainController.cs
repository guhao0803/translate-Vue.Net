/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹View_TransMainController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.System.IServices;
namespace VOL.System.Controllers
{
    [Route("api/View_TransMain")]
    [PermissionTable(Name = "View_TransMain")]
    public partial class View_TransMainController : ApiBaseController<IView_TransMainService>
    {
        public View_TransMainController(IView_TransMainService service)
        : base(service)
        {
        }
    }
}

