/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("Sys_TranslationType",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using VOL.System.IServices;
using Microsoft.AspNetCore.Authorization;

namespace VOL.System.Controllers
{
    public partial class Sys_TranslationTypeController
    {
        private readonly ISys_TranslationTypeService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        [ActivatorUtilitiesConstructor]
        public Sys_TranslationTypeController(
            ISys_TranslationTypeService service,
            IHttpContextAccessor httpContextAccessor
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }




        [HttpPost, Route("GetPageDataAllow"), AllowAnonymous]
        public ActionResult GetPageDataAllow([FromBody] PageDataOptions loadData)
        {
            return JsonNormal(Service.GetPageData(loadData));
        }
    }
}
