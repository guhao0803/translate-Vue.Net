/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹TransDetailController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.System.IServices;
namespace VOL.System.Controllers
{
    [Route("api/TransDetail")]
    [PermissionTable(Name = "TransDetail")]
    public partial class TransDetailController : ApiBaseController<ITransDetailService>
    {
        public TransDetailController(ITransDetailService service)
        : base(service)
        {
        }
    }
}

