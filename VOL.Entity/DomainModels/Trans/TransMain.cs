/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "翻译登记",TableName = "TransMain",DetailTable =  new Type[] { typeof(TransDetail)},DetailTableCnName = "翻译子表")]
    public partial class TransMain:BaseEntity
    {
        /// <summary>
       ///翻译内容主表
       /// </summary>
       [Key]
       [Display(Name ="翻译内容主表")]
       [Column(TypeName="uniqueidentifier")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public Guid TranId { get; set; }

       /// <summary>
       ///编号
       /// </summary>
       [Display(Name ="编号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string SerialNumber { get; set; }


        /// <summary>
        ///原语种
        /// </summary>
        [Display(Name = "类型")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? TransType { get; set; }

        /// <summary>
        ///申请人
        /// </summary>
        [Display(Name ="申请人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string Applicant { get; set; }

       /// <summary>
       ///原语种
       /// </summary>
       [Display(Name ="原语种")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? PrimitiveLanguage { get; set; }

       /// <summary>
       ///翻译语种
       /// </summary>
       [Display(Name ="翻译语种")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? TranslationLanguages { get; set; }

       /// <summary>
       ///公正内容
       /// </summary>
       [Display(Name ="公正内容")]
       [MaxLength(4000)]
       [Column(TypeName="nvarchar(4000)")]
       [Editable(true)]
       public string FairContent { get; set; }

       /// <summary>
       ///公正书号
       /// </summary>
       [Display(Name ="公正书号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string ISBN { get; set; }

       /// <summary>
       ///取证日期
       /// </summary>
       [Display(Name ="取证日期")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? EvidenceCollectionDate { get; set; }

       /// <summary>
       ///加急状态
       /// </summary>
       [Display(Name ="加急状态")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? UrgentStatus { get; set; }

       /// <summary>
       ///订单状态
       /// </summary>
       [Display(Name ="订单状态")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? OrderStatus { get; set; }

       /// <summary>
       ///备注内容
       /// </summary>
       [Display(Name ="备注内容")]
       [MaxLength(4000)]
       [Column(TypeName="nvarchar(4000)")]
       [Editable(true)]
       public string Remark { get; set; }

       /// <summary>
       ///创建人ID
       /// </summary>
       [Display(Name ="创建人ID")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? CreateID { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(255)]
       [Column(TypeName="varchar(255)")]
       [Editable(true)]
       public string Creator { get; set; }

       /// <summary>
       ///创建日期
       /// </summary>
       [Display(Name ="创建日期")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? CreateDate { get; set; }

       /// <summary>
       ///修改人ID
       /// </summary>
       [Display(Name ="修改人ID")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? ModifyID { get; set; }

       /// <summary>
       ///修改人
       /// </summary>
       [Display(Name ="修改人")]
       [MaxLength(255)]
       [Column(TypeName="varchar(255)")]
       [Editable(true)]
       public string Modifier { get; set; }

       /// <summary>
       ///修改日期
       /// </summary>
       [Display(Name ="修改日期")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? ModifyDate { get; set; }

       /// <summary>
       ///翻译稿件
       /// </summary>
       [Display(Name ="翻译稿件")]
       [MaxLength(4000)]
       [Column(TypeName="nvarchar(4000)")]
       [Editable(true)]
       public string TranslationManuscript { get; set; }

       /// <summary>
       ///分配翻译人
       /// </summary>
       [Display(Name ="分配翻译人")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? Assignor { get; set; }


        /// <summary>
        ///管理部门
        /// </summary>
        [Display(Name = "管理部门")]
        [Column(TypeName = "uniqueidentifier")]
        [Editable(true)]
        public Guid? ManageDept { get; set; }




        [Display(Name ="翻译子表")]
       [ForeignKey("TranId")]
       public List<TransDetail> TransDetail { get; set; }

    }
}