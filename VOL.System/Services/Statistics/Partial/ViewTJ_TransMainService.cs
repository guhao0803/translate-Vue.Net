/*
 *所有关于ViewTJ_TransMain类的业务代码应在此处编写
*可使用repository.调用常用方法，获取EF/Dapper等信息
*如果需要事务请使用repository.DbContextBeginTransaction
*也可使用DBServerProvider.手动获取数据库相关信息
*用户信息、权限、角色等使用UserContext.Current操作
*ViewTJ_TransMainService对增、删、改查、导入、导出、审核业务代码扩展参照ServiceFunFilter
*/
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;
using System.Linq;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using VOL.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.System.IRepositories;
using System;
using VOL.Core.ManageUser;

namespace VOL.System.Services
{
    public partial class ViewTJ_TransMainService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IViewTJ_TransMainRepository _repository;//访问数据库

        [ActivatorUtilitiesConstructor]
        public ViewTJ_TransMainService(
            IViewTJ_TransMainRepository dbRepository,
            IHttpContextAccessor httpContextAccessor
            )
        : base(dbRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _repository = dbRepository;
            //多租户会用到这init代码，其他情况可以不用
            //base.Init(dbRepository);
        }



        /*public override PageGridData<ViewTJ_TransMain> GetPageData(PageDataOptions options)

        {
            //查询table界面显示求和
            SummaryExpress = (IQueryable<ViewTJ_TransMain> queryable) =>
            {
                return queryable.GroupBy(x => 1).Select(x => new
                {
                    //AvgPrice注意大小写和数据库字段大小写一样
                    AllCost = x.Sum(o => o.AllCost).ToString("f2")
                })
                .FirstOrDefault();
            };
            return base.GetPageData(options);
        }*/


        public override PageGridData<ViewTJ_TransMain> GetPageData(PageDataOptions options)
        {

            QueryRelativeExpression = (IQueryable<ViewTJ_TransMain> queryable) =>
            {


                if (UserContext.Current.IsSuperAdmin)
                {
                    return queryable;
                }
                else
                {
                    var deptIds = UserContext.Current.GetAllChildrenDeptIds();
                    queryable = queryable.Where(x => deptIds.Contains(x.ManageDept ?? Guid.Parse("00000000-0000-0000-0000-000000000001")));

                }


                return queryable;
            };


            return base.GetPageData(options);
        }
    }
}
