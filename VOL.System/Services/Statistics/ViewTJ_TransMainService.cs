/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下ViewTJ_TransMainService与IViewTJ_TransMainService中编写
 */
using VOL.System.IRepositories;
using VOL.System.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.System.Services
{
    public partial class ViewTJ_TransMainService : ServiceBase<ViewTJ_TransMain, IViewTJ_TransMainRepository>
    , IViewTJ_TransMainService, IDependency
    {
    public ViewTJ_TransMainService(IViewTJ_TransMainRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IViewTJ_TransMainService Instance
    {
      get { return AutofacContainerModule.GetService<IViewTJ_TransMainService>(); } }
    }
 }
