/*
 *所有关于TransMain类的业务代码应在此处编写
*可使用repository.调用常用方法，获取EF/Dapper等信息
*如果需要事务请使用repository.DbContextBeginTransaction
*也可使用DBServerProvider.手动获取数据库相关信息
*用户信息、权限、角色等使用UserContext.Current操作
*TransMainService对增、删、改查、导入、导出、审核业务代码扩展参照ServiceFunFilter
*/
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;
using System.Linq;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using VOL.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.System.IRepositories;
using System;

namespace VOL.System.Services
{
    public partial class TransMainService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ITransMainRepository _repository;//访问数据库

        [ActivatorUtilitiesConstructor]
        public TransMainService(
            ITransMainRepository dbRepository,
            IHttpContextAccessor httpContextAccessor
            )
        : base(dbRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _repository = dbRepository;
            //多租户会用到这init代码，其他情况可以不用
            base.Init(dbRepository);
            IsMultiTenancy = true;
        }


        /// <summary>
        /// 设置弹出框明细表的合计信息
        /// </summary>
        /// <typeparam name="detail"></typeparam>
        /// <param name="queryeable"></param>
        /// <returns></returns>
        protected override object GetDetailSummary<detail>(IQueryable<detail> queryeable)
        {
            return (queryeable as IQueryable<TransDetail>).GroupBy(x => 1).Select(x => new
            {
                //Weight/Qty注意大小写和数据库字段大小写一样
                EstimatedCost = x.Sum(o => o.EstimatedCost)
            }).FirstOrDefault();
        }


        public override WebResponseContent Add(SaveModel saveDataModel)
        {

            AddOnExecuting = (TransMain item, object list) =>
            {

                // 获取当前日期的年、月、日
                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;
                int day = DateTime.Now.Day;
                string date = DateTime.Now.ToString("yyyy-MM-dd");
                var rep = repository.Find(x => ((DateTime)x.CreateDate).Date == DateTime.Today).Select(s => s.TranId);
                var maxNum = rep.Count() == 0 ? 1 : rep.Count() + 1;


                item.SerialNumber = $"{year}{month:00}{day:00}{maxNum.ToString().PadLeft(3, '0')}";

                return new WebResponseContent().OK();
            };


            return base.Add(saveDataModel);
        }
    }
}
