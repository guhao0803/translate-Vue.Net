/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下View_TransDetailService与IView_TransDetailService中编写
 */
using VOL.System.IRepositories;
using VOL.System.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.System.Services
{
    public partial class View_TransDetailService : ServiceBase<View_TransDetail, IView_TransDetailRepository>
    , IView_TransDetailService, IDependency
    {
    public View_TransDetailService(IView_TransDetailRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IView_TransDetailService Instance
    {
      get { return AutofacContainerModule.GetService<IView_TransDetailService>(); } }
    }
 }
