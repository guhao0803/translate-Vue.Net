/*
 *所有关于View_TransMain类的业务代码应在此处编写
*可使用repository.调用常用方法，获取EF/Dapper等信息
*如果需要事务请使用repository.DbContextBeginTransaction
*也可使用DBServerProvider.手动获取数据库相关信息
*用户信息、权限、角色等使用UserContext.Current操作
*View_TransMainService对增、删、改查、导入、导出、审核业务代码扩展参照ServiceFunFilter
*/
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;
using System.Linq;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using VOL.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.System.IRepositories;
using System;
using VOL.Core.UserManager;
using VOL.Core.ManageUser;

namespace VOL.System.Services
{
    public partial class View_TransMainService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IView_TransMainRepository _repository;//访问数据库

        [ActivatorUtilitiesConstructor]
        public View_TransMainService(
            IView_TransMainRepository dbRepository,
            IHttpContextAccessor httpContextAccessor
            )
        : base(dbRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _repository = dbRepository;
            //多租户会用到这init代码，其他情况可以不用
            //base.Init(dbRepository);
        }


        /// <summary>
        /// 设置弹出框明细表的合计信息
        /// </summary>
        /// <typeparam name="detail"></typeparam>
        /// <param name="queryeable"></param>
        /// <returns></returns>
        protected override object GetDetailSummary<detail>(IQueryable<detail> queryeable)
        {
            return (queryeable as IQueryable<View_TransDetail>).GroupBy(x => 1).Select(x => new
            {
                //Weight/Qty注意大小写和数据库字段大小写一样
                EstimatedCost = x.Sum(o => o.EstimatedCost)
            }).FirstOrDefault();
        }


        public override PageGridData<View_TransMain> GetPageData(PageDataOptions options)
        {
             
            //查询前可以自已设定查询表达式的条件
            QueryRelativeExpression = (IQueryable<View_TransMain> queryable) =>
            {


                if (UserContext.Current.IsSuperAdmin)
                {
                    return queryable;
                }
                else
                {
                    var deptIds = UserContext.Current.GetAllChildrenDeptIds();
                    queryable = queryable.Where(x => deptIds.Contains(x.ManageDept ?? Guid.Parse("00000000-0000-0000-0000-000000000001")));

                }


                return queryable;
            };




            return base.GetPageData(options);
        }


        public WebResponseContent Select_Assignee(SaveModel saveDataModel)
        {

            View_TransMain view_Trans = new View_TransMain();

            view_Trans.TranId = Guid.Parse(saveDataModel.MainData["TranId"].ToString());


            view_Trans.Assignor = int.Parse(saveDataModel.MainData["Assignor"].ToString());

            view_Trans.OrderStatus = 5;


            repository.Update(view_Trans, x => new { x.Assignor, x.OrderStatus }, true);
            //其他从表按上面同样的操作即可
            //最终保存
            //repository.SaveChanges();
            return WebResponseContent.Instance.OK("分配完成");
        }
    }
}
