/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下View_TransMainService与IView_TransMainService中编写
 */
using VOL.System.IRepositories;
using VOL.System.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.System.Services
{
    public partial class View_TransMainService : ServiceBase<View_TransMain, IView_TransMainRepository>
    , IView_TransMainService, IDependency
    {
    public View_TransMainService(IView_TransMainRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IView_TransMainService Instance
    {
      get { return AutofacContainerModule.GetService<IView_TransMainService>(); } }
    }
 }
