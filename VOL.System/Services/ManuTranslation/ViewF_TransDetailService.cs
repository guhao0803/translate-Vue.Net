/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下ViewF_TransDetailService与IViewF_TransDetailService中编写
 */
using VOL.System.IRepositories;
using VOL.System.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.System.Services
{
    public partial class ViewF_TransDetailService : ServiceBase<ViewF_TransDetail, IViewF_TransDetailRepository>
    , IViewF_TransDetailService, IDependency
    {
    public ViewF_TransDetailService(IViewF_TransDetailRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IViewF_TransDetailService Instance
    {
      get { return AutofacContainerModule.GetService<IViewF_TransDetailService>(); } }
    }
 }
