/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下ViewF_TransMainService与IViewF_TransMainService中编写
 */
using VOL.System.IRepositories;
using VOL.System.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.System.Services
{
    public partial class ViewF_TransMainService : ServiceBase<ViewF_TransMain, IViewF_TransMainRepository>
    , IViewF_TransMainService, IDependency
    {
    public ViewF_TransMainService(IViewF_TransMainRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IViewF_TransMainService Instance
    {
      get { return AutofacContainerModule.GetService<IViewF_TransMainService>(); } }
    }
 }
