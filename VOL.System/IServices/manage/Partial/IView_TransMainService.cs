/*
*所有关于View_TransMain类的业务代码接口应在此处编写
*/
using VOL.Core.BaseProvider;
using VOL.Entity.DomainModels;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace VOL.System.IServices
{
    public partial interface IView_TransMainService
    {


        WebResponseContent Select_Assignee(SaveModel saveModel);

    }
 }
